###########################################
# Define Variable 
###########################################
variable "vsphere_user" {}
variable "vsphere_password" {}
variable "vsphere_server" {}

################################################
# Provider section
################################################
provider "vsphere" {
  user           = "${var.vsphere_user}"
  password       = "${var.vsphere_password}"
  vsphere_server = "${var.vsphere_server}"

  # If you have a self-signed cert
  allow_unverified_ssl = true
}

#################################################
#Capturing the data from vsphere
#################################################
data "vsphere_datacenter" "dc" {
  name = "CHDC-DC-DEV"
}

data "vsphere_compute_cluster" "cluster" {
    name          = "CHDC-CLU-DEV-WIN-005"
    datacenter_id = "${data.vsphere_datacenter.dc.id}"
}
data "vsphere_datastore" "datastore" {
  name          = "CHDC-DSCLU-DEV-WIN-005"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_network" "network" {
  name          = "vlan3154-gc-uat-app-win"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}


########################################################
# sourcing template
#############################################################
data "vsphere_virtual_machine" "template" {
  name          = "TMPLT-CHDC-DEV-MZ-WIN2019"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

#########################################################
# Resource
##########################################################


######
#See if tags work
#####

resource "vsphere_tag" "environment" {
    name        = "DEV"
    category_id = "${vsphere_tag_category.environment.id}"
}

resource "vsphere_tag" "os" {
    name         = "Windows"
    category_id = "${vsphere_tag_category.os.id}"
}




resource "vsphere_virtual_machine" "cloned_virtual_machine" {
  name             = "ch1vmwwdvapp999"
  folder           = "Discovered virtual machine"
  resource_pool_id = "${data.vsphere_compute_cluster.cluster.resource_pool_id}"
  datastore_id     = "${data.vsphere_datastore.datastore.id}"
  num_cpus = "${data.vsphere_virtual_machine.template.num_cpus}"
  memory   = "${data.vsphere_virtual_machine.template.memory}"
  guest_id = "${data.vsphere_virtual_machine.template.guest_id}"
  annotation       = "Deploy via Terraform" # should add notes to VM
  guest_id = "${data.vsphere_virtual_machine.template.guest_id}"
  scsi_type = "${data.vsphere_virtual_machine.template.scsi_type}"
  
  network_interface {
    network_id = "${data.vsphere_network.network.id}"
     adapter_type = "${data.vsphere_virtual_machine.template.network_interface_types[0]}"
  }

  disk {
        label            = "disk0"
        size             = data.vsphere_virtual_machine.template.disks.0.size
        # settings for Thick Provision Lazy Zeroed below
        thin_provisioned = false
        eagerly_scrub = false
    }

  tags = [
        "${vsphere_tag.environment.id}",
        "${vsphere_tag.os.id}",
    ]


################################################################
# Initiate the clone 
#################################################################
clone {
  template_uuid = "${data.vsphere_virtual_machine.template.id}"

  customize {
    windows_options {
      host_name = "ch1vmwwdvapp999a"
      #domain    = "uboc-ad.uboc.com"
      run_once_command_list = [Powershell –ExecutionPolicy Bypass "\\chdc-ese-pvvc3\D$\Temp\vmwareTest.ps1 -CFEUser var.username -ExpectedName var.vmName"]
    }
    
    network_interface {
      ipv4_address = "10.25.150.20" #need valid IP
      ipv4_netmask = "23"
    }

    ipv4_gateway = "10.25.150.1"
  }
 }
}



######################################
# Output - may not work
######################################
output "my_ip_address" {
 value = "${vsphere_virtual_machine.vm.dipv4_address}"
}